# **PentagoMMO** #

## Technologies & Frameworks ##

* CoffeeScript
* Unity
* Node.js
* Angular.js
* MongoDB
* Gulp

## How to start application? ##

### API (Server) ###

Install **Gulp, Node.js, MongoDB**

Don't forget to run **`npm install`** from `Web\api` folder

*Primary*

* **`gulp scripts`** - compile scripts into `.tmp`
* **`npm start`** - start server application!

*All*

* **`gulp watch`** - watch about coffee files and compile them after each changes
* **`gulp`** - clean up `.tmp` folder and compile scripts there
* **`gulp clean`** - remove **ALL** content from `.tmp` folder
* **`gulp scripts`** - compile scripts into `.tmp`
domain = require 'domain'
express = require 'express'
log = require 'winston'
_ = require 'lodash'

db = require './core/mongoose'

setupHttpDefaultMiddleware = (app)->
  path = require('path')
  logger = require('morgan')
  cookieParser = require('cookie-parser')

  app.use logger('dev')
  app.use cookieParser()

  app.use express.static path.join __dirname, './.tmp/app-ui/'

initDatabase = ->
  db.init (err) ->
    log.info "Models are loaded!" unless err

appBuilder = ->
  app = express()
  _with = (fn)->
    ->
      fn app if _.isFunction fn
      @

  instance: app
  withDefaultHttpMiddleware: _with(setupHttpDefaultMiddleware)
  withInitDatabase: _with(initDatabase)

module.exports = appBuilder

module.exports.defaultSetup = ->
  appBuilder()
  .withDefaultHttpMiddleware()
  .withInitDatabase()
  .instance
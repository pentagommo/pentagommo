config = require '../core/appconfig'
log = require 'winston'
app = require('../app').defaultSetup()

port = process.env.PORT || config.get('port') || 3000
app.set('port', port)

server = app.listen port, ->
  log.info('Express server listening on port ' + server.address().port)


nconf = require 'nconf'

getEnvConfig = (envName) ->
  './config.' + (envName || 'development') + '.json'

nconf.argv()
.env()
.file('custom', getEnvConfig(process.env.NODE_ENV))
.file('default', './config.json')

module.exports = nconf

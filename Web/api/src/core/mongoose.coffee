mongoose = require 'mongoose'
config = require './appconfig'
log = require 'winston'

mongoose.connect config.get 'mongoose:uri'

db = mongoose.connection

db.on "error", (err) ->
  log.error "Connection error:", err.message

db.once "open", ->
  log.info "Connected to DB!"

models = []

module.exports.models = models
module.exports.init = (cb)->
  userModel = 'user'
  try
    models[userModel] = mongoose.model userModel, require('./models/user.model.coffee').Scheme
  catch e
    cb e

  cb()
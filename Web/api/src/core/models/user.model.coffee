q = require('q')
mongoose = require('mongoose')

userSchema = mongoose.Schema
  username:
    type: String
    required: yes
    unique: yes
  passwordHash:
    type: String
    required: yes
, collection: 'users'

module.exports = mongoose.model("User", userSchema)
'use strict';

var gulp = require('gulp');

var pentagoConfig = require('./pentago.config.json')

var $ = require('gulp-load-plugins')({
    pattern: ['gulp-*', 'main-bower-files', 'uglify-save-license', 'del']
});

gulp.task('default', ['clean'], function () {
    gulp.start('scripts');
});

gulp.task('watch', function () {
    gulp.start('watchers');
});

gulp.task('clean', function () {
    $.del(['.tmp/**'],
        {force: true}).then(function (paths){
            //console.log("Deleted folders/files:\n", paths.join('\n'));
        });
});

gulp.task('watchers', ['scripts'], function () {
    gulp.watch('src/**/*.coffee', ['scripts'], function () {
    });
});

gulp.task('scripts', function () {
    return gulp.src("./src/**/*.coffee")
        .pipe($.cached('coffeeScripts'))
        .pipe($.plumber())
        .pipe($.coffeelint('coffeelint.json'))
        .pipe($.coffeelint.reporter())
        .pipe($.sourcemaps.init())
        .pipe($.coffee({
            bare: false
        }))
        .pipe($.sourcemaps.write('.'))
        .pipe(gulp.dest('.tmp/'))
        .pipe($.size())
        ;
});